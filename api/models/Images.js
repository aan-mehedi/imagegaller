/**
 * Created by ASUS on 10-Feb-17.
 */
module.exports = {

  schema: true,

  attributes: {
    url:{
      type:'string',
      required:true
    },
    point:{
      type: 'string'
    },
    deleteFlag:{
      type:'boolean'
    },
    //reference to subfolder
    subFolderId:{
      model:'subfolders'
    }
  }
};
