/**
 * Created by ASUS on 10-Feb-17.
 */
module.exports = {

  schema: true,

  attributes: {
    name:{
      type:'string',
      required:true
    },
    //reference to images
    image: {
      collection: 'images',
      via: 'subFolderId'
    }
  }
};
