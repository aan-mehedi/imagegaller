/**
 * Created by Suzon Das on 10-Feb-17.
 */
/**
 * @file ng-gallery-app
 * Filterable Gallery App
 * @description This app has been developed as a aptitude test for MEAN stack
 * This ng-app controls and functions all the task related in page element rendering
 *
 * @link : https://docs.angularjs.org/tutorial
 */
'use strict';
/*jshint esversion: 6 */
var app = angular.module('galleryApp', ['ngRoute']); //Declaring Gallery App

/*****************************************Main Controller**************************************/
app.controller('mainCtrl', function ($scope, $route, subFolderService, updatePointService) {

  /**
   * @description initializing all image array
   * @scope
   * @type {Array}
   */
  $scope.allSubfoldersWithImages = [];

  /**
   * @description Listing sub folders images and rendering in image grid
   * @constructor
   * @param {undefined}
   * @returns {undefined}
   */
  $scope.subFoldersWithImages = function () {
    subFolderService.subFoldersWithImage().then(function (data) {
      $scope.allSubfoldersWithImages = data;
      $route.reload();
    });
  };

  $scope.subFoldersWithImages(); // Initiate Sub Folders with images

  /**
   * @description Listing sub folders images and rendering in image grid
   * @constructor
   * @param {integer} index this is the index value of the image clicked
   * @param {integer} parentIndex this is the index value of the image clicked
   * for archiving
   * @returns {undefined}
   */
  $scope.archive = function (index, parentIndex) {
    console.log(index, parentIndex);
    var addingPointImagesId = [];
    var deleteImageId = $scope.allSubfoldersWithImages[parentIndex].image[index].id;
    for (var i = 0; i < parentIndex + 1; i++) {
      var loopBreak = false;
      for(var j=0; j<$scope.allSubfoldersWithImages[i].image.length; j++){
        addingPointImagesId.push($scope.allSubfoldersWithImages[i].image[j].id);
        if($scope.allSubfoldersWithImages[i].image[j].id === deleteImageId){
          loopBreak = true;
        }
      }
     if(loopBreak){
        break;
     }
    }
    var updateData = {};
    updateData.addingPointImagesId = addingPointImagesId;
    updateData.deleteImageId = deleteImageId;

    /**
     * @description updating the points and archiving the image
     * @methodOf updatePointService Service
     * @param {object} data includes two array. One is for the archiving image Id and another is for
     * previous images Id to be calculated for point addition
     * @returns {undefined}
     */
    updatePointService.update(updateData).then(function (data) {
      $scope.subFoldersWithImages(); //Refreshing the image grid
    });
  };

  /**
   * @scope Changing filter value when user set new value for filter
   */
  $scope.changeFilterValue = function () {
    $("#filterRefresh").trigger('click');
    $scope.dataforFilter = '.point' + $scope.imageFilterValue;
  };
});

/*************************************************End Main Controller****************************/

/**********************************************Services for Gallery App************************/

/**
 * @service SubfolderService for managing all functions needed in subfolder listing and
 * fetching images for subfolders
 */
app.service('subFolderService', function ($http, $q) {
  /**
   * @description fetching all images
   * @param {undefined}
   * @return {function} promise returning sub folder's images
   */
  this.subFoldersWithImage = function () {
    var defer = $q.defer();
    $http.post('/getSubfoldersWithImage').then(function successCallback(resp) {
      defer.resolve(resp.data);
    }, function errorCallback(err) {
      defer.reject(err);
    });
    return defer.promise;
  };
});

/**
 * @service updatePointService for managing all functions needed
 * for updating image data including point and archiving
 */
app.service('updatePointService', function ($http, $q) {
  this.update = function (data) {
    var defer = $q.defer();
    $http.post('/updatePoint', data).then(function successCallback(resp) {
      defer.resolve(resp.data);
    }, function errorCallback(err) {
      defer.reject(err);
    });
    return defer.promise;
  };

});

/**********************************************End Services for Gallery App************************/

/***********************************LazyLoad Vendor setting**************************************/
app.service(
  'scrollAndResizeListener', function ($window, $document, $timeout) {
    var id = 0,
      listeners = {},
      scrollTimeoutId,
      resizeTimeoutId;

    function invokeListeners() {
      var clientHeight = $document[0].documentElement.clientHeight,
        clientWidth = $document[0].documentElement.clientWidth;

      for (var key in listeners) {
        if (listeners.hasOwnProperty(key)) {
          listeners[key](clientHeight, clientWidth); // call listener with given arguments
        }
      }
    }


    $window.addEventListener('scroll', function () {
      // cancel previous timeout (simulates stop event)
      $timeout.cancel(scrollTimeoutId);

      // wait for 200ms and then invoke listeners (simulates stop event)
      scrollTimeoutId = $timeout(invokeListeners, 200);
    });


    $window.addEventListener('resize', function () {
      $timeout.cancel(resizeTimeoutId);
      resizeTimeoutId = $timeout(invokeListeners, 200);
    });


    return {
      bindListener: function (listener) {
        var index = ++id;

        listeners[id] = listener;

        return function () {
          delete listeners[index];
        }
      }
    };
  }
);

app.directive(
  'imageLazySrc', function ($document, scrollAndResizeListener) {
    return {
      restrict: 'A',
      link: function ($scope, $element, $attributes) {
        var listenerRemover;

        function isInView(clientHeight, clientWidth) {
          // get element position
          var imageRect = $element[0].getBoundingClientRect();

          if (
            (imageRect.top >= 0 && imageRect.bottom <= clientHeight)
            &&
            (imageRect.left >= 0 && imageRect.right <= clientWidth)
          ) {
            $element[0].src = $attributes.imageLazySrc; // set src attribute on element (it will load image)

            // unbind event listeners when image src has been set
            listenerRemover();
          }
        }

        // bind listener
        listenerRemover = scrollAndResizeListener.bindListener(isInView);

        // unbind event listeners if element was destroyed
        // it happens when you change view, etc
        $element.on('$destroy', function () {
          listenerRemover();
        });


        // explicitly call scroll listener (because, some images are in viewport already and we haven't scrolled yet)
        isInView(
          $document[0].documentElement.clientHeight,
          $document[0].documentElement.clientWidth
        );
      }
    };
  }
);
/************************************************End LazyLoad**************************************/

/***********************************************Gallery Route*************************************/
app.config(function($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl : "/templates/gallery.html"
    })
});
/*********************************************End Gallery Route*************************************/
