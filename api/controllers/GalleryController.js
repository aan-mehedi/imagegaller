/**
 * Created by Suzon Das on 10-Feb-17.
 */
/**
 * Gallery Controller
 *
 * @description :: Server-side logic for managing Gallery app
 * It covers all the client side data processes for Gallery module
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
'use strict';
/*jshint esversion: 6 */
module.exports = {
  /**
   * @description Landing slug's method
   * @method
   * @param req
   * @param res
   */
  index: function (req, res) {
    res.view('gallery/index');
  },
  /**
   * @description Updating Image points, Archiving Image
   * @method
   * @param req
   * @param res
   */
  updatePoint: function (req, res) {
    var async = require('async');
    var addingPointImagesId = req.param('addingPointImagesId');
    var deleteImageId = req.param('deleteImageId');
    var trackAction = [];
    addingPointImagesId.forEach(function (eachItem) {
      trackAction.push(function (callback) {
        Images.findOne({id: eachItem}).exec(function (err, data) {
          var updatedData = data;
          updatedData.point = Number(data.point) + Number(1);
          if (!!data.deleteFlag && data.deleteFlag === true) {
            callback(null, {'id': data.id, 'point': data.point});
          }
          else {
            if (data.id === deleteImageId) {
              updatedData.point = Number(0);
              updatedData.deleteFlag = true;
            }
            Images.update({id: data.id}, updatedData).exec(function (err, updatedData) {
              if (err) {
                return res.serverError(err);
              }
              callback(null, {'id': updatedData.id, 'point': updatedData.point});
            });
          }
        });
      })
    });
    async.parallel(trackAction, function (err, result) {
      if (err) {
        return res.serverError(err);
      }
      if (result) {
        res.send(result);
      }
    })
  },
  /**
   * @description Fetching all Subfolder details
   * @method
   * @param req
   * @param res
   */
  getSubfoldersWithImage: function (req, res) {
    Subfolders.find()
      .populate('image') //association to the Images
      .exec(function (err, data) {
        if (err) {
          return res.serverError(err);
        }
        if (data) {
          res.send(data);
        }
        if(!data){
          res.send('No Images Found !')
        }
      });
  }
};
